import sys
import os
import django
import json
sys.path.append("../webapp") #here store is root folder(means parent).
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "invaana_apis.settings")
django.setup()
from projects.models import Project, Tag
from community.models import CommunityInterest, TechnologyInterest


def read_json(f):
    with open(f) as data:
        return json.load(data)
    return None

projects = read_json('json/playground-projects.json')
interests = read_json('json/interests.json')
domains = read_json('json/domains.json')


def import_projects():
    for project in projects['data']:
        print project
        p, created = Project.objects.get_or_create(title=project['title'])
        if created:
            p.description = project.get('description')
            p.github_url = project.get('github_url')
            p.save()

            for tag in project['tags']:
                t, c = Tag.objects.get_or_create(title=tag)
                p.tags.add(t)


def import_domains():
    for interest in domains['data']:
        print interest
        i, created = CommunityInterest.objects.get_or_create(title=interest['title'])
        if created:
            for tag in interest['tags']:
                t, c = Tag.objects.get_or_create(title=tag)
                i.tags.add(t)


def import_interests():
    for interest in interests['data']:
        print interest
        i, created = TechnologyInterest.objects.get_or_create(title=interest['title'])
        i.description = interest.get('description')
        i.license = interest.get('license')
        i.doc_url = interest.get('documentation')
        i.github_url = interest.get('github_url')
        i.get_started = interest.get('get_started')
        i.save()

# import_projects()
# import_domains()
import_interests()