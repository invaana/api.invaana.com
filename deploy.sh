#!/usr/bin/env bash

# cleaning and creating the webapp folder
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "mkdir -p ~/webapp"
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "rm -rf ~/webapp/*"

# creating temp files
tar -zcvf archive.tar.gz webapp/*
tar -zcvf requirements.tar.gz requirements/*

# uploading the temp files
scp -i ~/Projects/keys/gcloud_invaanaorg_ravi -r archive.tar.gz ravi.merugu@35.197.130.147:/home/ravi.merugu/webapp/
scp -i ~/Projects/keys/gcloud_invaanaorg_ravi -r requirements.tar.gz ravi.merugu@35.197.130.147:/home/ravi.merugu/webapp/
scp -i ~/Projects/keys/gcloud_invaanaorg_ravi -r Dockerfile ravi.merugu@35.197.130.147:/home/ravi.merugu/webapp

# cleaning the temp files
rm archive.tar.gz
rm requirements.tar.gz

ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "cd ~/webapp && tar xvzf archive.tar.gz"
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "cd ~/webapp && tar xvzf requirements.tar.gz"

# start docker
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "sudo docker stop invaana-apis && sudo docker rm invaana-apis"
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "cd ~/webapp && sudo docker build -t invaana-apis-image --build-arg build_env='production' -f Dockerfile . --no-cache"
ssh -i ~/Projects/keys/gcloud_invaanaorg_ravi ravi.merugu@35.197.130.147 -p22 "cd ~/webapp && sudo docker run --name invaana-apis  -d -p 8000:8000 invaana-apis-image"
