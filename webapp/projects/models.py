# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from member.models import UserProfile
from django.contrib.auth import get_user_model
User = get_user_model()
# Create your models here.


class Project(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(default=None, null=True, blank=True)
    tags = models.ManyToManyField('topics.Tag')
    description = models.TextField(default=None, null=True, blank=True)
    github_url = models.URLField(default=None, null=True, blank=True)
    owner = models.ForeignKey(UserProfile, default=None, null=True)

    def __unicode__(self):
        return self.title