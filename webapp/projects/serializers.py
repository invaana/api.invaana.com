from .models import Project
from rest_framework import serializers
from topics.serializers import TagSerializer
from member.serializers import UserProfileSerializer


class ProjectSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    owner = UserProfileSerializer()

    class Meta:
        model = Project
        fields = ["id", "title", "description", "github_url", "tags", 'owner']