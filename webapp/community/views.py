# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import viewsets
from .models import Community
from .serializers import CommunitySerializers
# Create your views here.


class CommunityViewSet(viewsets.ModelViewSet):
    serializer_class = CommunitySerializers
    queryset = Community.objects.all()