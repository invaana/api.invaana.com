from .models import Community
from rest_framework import serializers
from member.serializers import UserProfileSerializer


class CommunitySerializers(serializers.ModelSerializer):
    type = serializers.CharField(source='get_type_display', read_only=True)
    members = UserProfileSerializer(many=True, read_only=True)
    moderators = UserProfileSerializer(many=True, read_only=True)
    owner = UserProfileSerializer(read_only=True)

    class Meta:
        model = Community
        fields = ['id', 'title', 'slug', 'image', 'type', 'location', 'description', 'members',
                  'owner', 'moderators']

