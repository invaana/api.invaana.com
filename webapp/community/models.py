# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.contrib.auth import get_user_model
from member.models import UserProfile
# Create your models here.


class Community(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    image = models.ImageField(default=None, null=True, blank=True)
    type = models.IntegerField(
        choices= (
            (1, "Technology"),
            (2, "Scientific"),
            (3, "Technology & Scientific")
        ),
        null=True,
        blank=True,
        default=None
    )
    location = models.CharField(default=None, null=True, blank=True, max_length=60)
    description = models.TextField(default=None, null=True, blank=True)
    members = models.ManyToManyField(UserProfile, through='CommunityMemberShip')
    moderators = models.ManyToManyField(UserProfile, through="ModeratorMemberShip", related_name="community_moderatorship")
    owner = models.ForeignKey(UserProfile, default=None, null=True, related_name="community_ownership")

    def __unicode__(self):
        return self.title


class CommunityMemberShip(models.Model):
    community = models.ForeignKey(Community)
    profile = models.ForeignKey(UserProfile)
    created_at = models.DateTimeField(default=timezone.now, blank=True, null=True)

    def __unicode__(self):
        return "%s - %s" %(self.community.title, self.member.user.first_name)


class ModeratorMemberShip(models.Model):
    community = models.ForeignKey(Community)
    profile = models.ForeignKey(UserProfile)
    created_at = models.DateTimeField(default=timezone.now, blank=True, null=True)

    def __unicode__(self):
        return "%s - %s" %(self.community.title, self.member.user.first_name)