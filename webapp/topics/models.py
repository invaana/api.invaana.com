# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify

# Create your models here.


class Tag(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField()

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.slug is None:
            self.slug = slugify(self.title)
        return super(Tag, self).save(*args, **kwargs)


class CommunityInterest(models.Model):
    title = models.CharField(max_length=255)
    tags = models.ManyToManyField('topics.Tag')

    def __unicode__(self):
        return self.title


class TechnologyInterest(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(default=None, null=True)
    license = models.CharField(max_length=100, null=True, default=None)
    homepage = models.URLField()
    github_url = models.URLField(default=None, null=True)
    doc_url = models.URLField(default=None, null=True)
    get_started_url = models.URLField(default=None, null=True)

    def __unicode__(self):
        return self.title