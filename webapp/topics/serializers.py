from rest_framework import serializers
from .models import CommunityInterest, TechnologyInterest, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['title', 'slug']


class TechnologyInterestSerializer(serializers.ModelSerializer):

    class Meta:
        model = TechnologyInterest
        fields = ['title', 'description', 'license', 'homepage', 'github_url', 'doc_url', 'get_started_url']


class CommunityInterestSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = CommunityInterest
        fields = ['title', 'tags']


