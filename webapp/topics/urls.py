from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^technologies$', views.TechnologyInterestViewSet.as_view({'get':'list'})),
    url(r'^interests$', views.CommunityInterestViewSet.as_view({'get':'list'})),
]
