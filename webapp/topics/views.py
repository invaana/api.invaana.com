# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TechnologyInterestSerializer, CommunityInterestSerializer
from .models import TechnologyInterest, CommunityInterest
# Create your views here.


class TechnologyInterestViewSet(viewsets.ModelViewSet):
    queryset = TechnologyInterest.objects.all()
    serializer_class = TechnologyInterestSerializer


class CommunityInterestViewSet(viewsets.ModelViewSet):
    queryset = CommunityInterest.objects.all()
    serializer_class = CommunityInterestSerializer