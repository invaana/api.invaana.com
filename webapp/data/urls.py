from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^apis/beta/public/es$', views.ElasticSearchPublicView.as_view()),
    url(r'^$', views.HelloApiView.as_view()),
]
