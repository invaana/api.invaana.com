# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import views, response, permissions
from django.conf import settings
import requests as r


# Create your views here.


class ElasticSearchPublicView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        index_name = request.GET.get('index_name', None)
        doc_type = request.GET.get('doc_type', None)
        q = request.GET.get('q', "")
        if index_name:
            if index_name and doc_type: es_url = "{0}/{1}/{2}/_search?{3}".format(
                settings.ELASTICSEARCH_URL, index_name, doc_type, q)
            else: es_url = "{0}/{1}/_search?{2}".format(settings.ELASTICSEARCH_URL, index_name, q)
            req = r.get(es_url)
            return response.Response({"message": "Data gathered successfully", "data": req.json()})
        else:
            return response.Response({"message": "No index requests provided to find the data; contact admin",
                                      "data": None}, status=400)


class HelloApiView(views.APIView):
    def get(self, request, format=None):
        return response.Response({"message": "Hello World!!"})
