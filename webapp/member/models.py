# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from allauth.account.signals import user_signed_up
from django.dispatch import receiver


class UserProfile(models.Model):
    user = models.OneToOneField(User,  related_name='profile')
    avatar = models.URLField(default=None, null=True, blank=True)
    location = models.CharField(max_length=30, blank=True)

    def __unicode__(self):
        return self.user.username



@receiver(user_signed_up)
def retrieve_social_data(request, user, sociallogin=None, **kwargs):
    """Signal, that gets extra data from sociallogin and put it to profile."""
    # get the profile where i want to store the extra_data
    print sociallogin.account.provider  # e.g. 'twitter'
    print sociallogin.account.get_avatar_url()
    print sociallogin.account.get_profile_url()
    if sociallogin:
        avatar_url = sociallogin.acc22ount.get_avatar_url()

    profile = UserProfile(user=user, avatar = avatar_url)
    # in this signal I can retrieve the obj from SocialAccount
    profile.save()