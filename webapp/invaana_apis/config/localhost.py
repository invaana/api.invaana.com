DEBUG = True
ALLOWED_HOSTS = ['*']
ELASTICSEARCH_URL = "http://127.0.0.1:9200"
CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 12
}