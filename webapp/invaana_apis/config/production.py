DEBUG = False
ALLOWED_HOSTS = [ 'api.invaana.com', 'api.invaana.org']
ELASTICSEARCH_URL = "http://10.148.0.2:9200"

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 12
}
CORS_ORIGIN_WHITELIST = ['invaana.org', 'invaana.com', 'invaana.github.io']