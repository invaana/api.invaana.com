FROM python:2.7
MAINTAINER Ravi RT Merugu <ravi.merugu@invaana.com>
ENV PYTHONUNBUFFERED 1

ARG build_env
ENV BUILD_ENV ${build_env}

RUN [ -d /webapp ] || mkdir /webapp;

ADD ./webapp /webapp/
ADD ./requirements /requirements/
RUN pip install -r /requirements/requirements.txt
RUN pip install -r /requirements/requirements-prod.txt
WORKDIR /webapp
RUN python manage.py migrate --noinput

EXPOSE 8000
CMD uwsgi --http :8000 --wsgi-file invaana_apis/wsgi.py --enable-threads